Require Import Arith.

Definition is_zero (n:nat) :=
    match n with
      0 => true
    | S p => false
    end.

Fixpoint sum_n n :=
  match n with
      0 => 0
    | S p => n + sum_n p
  end.
(*
Fixpoint rec_bac n :=
  match n with 0 => 0 | S p => rec_bac (S p) end.
*)

Fixpoint factorial n :=
  match n with
    0 => 1
  | S p => n * (factorial p)
  end.

Eval compute in factorial 5.

Eval compute in sum_n 5.

Fixpoint evenb n := 
  match n with 
    0 => true
  | 1 => false
  | S (S p) => evenb p
  end.

Eval compute in evenb 10.

Require Import List.

Fixpoint sum_list l :=
  match l with
    nil => 0
  | a::ts => a + sum_list ts
  end.

Eval compute in sum_list (2::3::4::5::6::nil).

Fixpoint insert n l :=
  match l with
    nil => n::nil
  | x::xs => if (leb n x) then n::x::xs else x::(insert n xs)
  end.

Eval compute in insert 4 (0::2::3::4::6::10::nil).

Fixpoint sorted l :=
  match l with
    nil => nil
  | x::xs => insert x (sorted xs)
  end.

Eval compute in sorted (10::3::9::0::5::6::nil).

Fixpoint exercise2a l :=
  match l with
    nil => true
  | a::nil => true
  | a::b::xs => leb a b
  end.

Fixpoint is_sorted l :=
  match l with
    nil => true
  | x::nil => true
  | x::y::xs => andb (leb x y) (is_sorted (y::xs))
  end.

Eval compute in is_sorted (1::3::4::nil).