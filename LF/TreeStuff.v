 Inductive TreeWithout : Type :=
  | Empty
  | Branch (l : TreeWithout) (r : TreeWithout).

Inductive TreeWith (T : Type) : Type :=
  | BranchHere (t : T) (l : TreeWithout) (r : TreeWithout)
  | BranchLeft (l : (TreeWith T)) (r : TreeWithout)
  | BranchRight (l : TreeWithout) (r : (TreeWith T)).
